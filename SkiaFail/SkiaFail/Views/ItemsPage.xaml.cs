﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SkiaFail.Models;
using SkiaFail.Views;
using SkiaFail.ViewModels;
using SkiaSharp.Views.Forms;
using SkiaFail.Drawing;
using SkiaSharp;
using Xamarin.Essentials;
using SkiaScene;
using SkiaScene.TouchManipulation;
using SkiaFail.Renderers;
using System.Diagnostics;
using TouchTracking;

namespace SkiaFail.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;

        // Add code for SkiaScene for pinch and zoom...
        private ISKScene _scene;
        private ITouchGestureRecognizer _touchGestureRecognizer;
        private ISceneGestureResponder _sceneGestureResponder;
        // ============

        float dimLen { get; set; }
        float dimWidth { get; set; }

        List<Gridline> completedGridlines = new List<Gridline>();
        public SKMatrix skMatrix { get; set; }
        public SKPoint PathSelectedPoint { get; set; }
        SKCanvas canvas;

        SKPaint paintDraw { get; set; }
        SKPaint paintBox { set; get; }

        SKPaint paintText = new SKPaint
        {
            TextSize = 32,
            Color = SKColors.Black,
            IsAntialias = true,
            IsStroke = false,
            Style = SKPaintStyle.Fill 
        };

        double density { get; set; }
        int minx { get; set; }
        int miny { get; set; }
        int maxx { get; set; }
        int maxy { get; set; }

        float scale { get; set; }
        float transX;
        float transY;

        float smallIncrement { get; set; }
        float largeIncrement { get; set; }
        int incrementMultiple { get; set; }  // how many smallIncrement fit in a largeIncrement

        private float _length { get; set; }
        public float skLength
        {
            get { return _length; }
            set
            {
                if (value != _length)
                {
                    _length = value;
                    OnPropertyChanged("skLength");
                }
            }
        }

        private float _width { get; set; }
        public float skWidth
        {
            get { return _width; }
            set
            {
                if (value != _width)
                {
                    _width = value;
                    OnPropertyChanged("skWidth");
                }
            }
        }

        private Color _selectedColor { get; set; }
        public Color SelectedColor
        {
            get { return _selectedColor; }
            set
            {
                if (value != _selectedColor)
                {
                    _selectedColor = value;
                    OnPropertyChanged("SelectedColor");
                }
            }
        }

        public ItemsPage()
        {
            InitializeComponent();
            Init();

            BindingContext = this;
        }


        private void Init()
        {
            // re Xamarin essentials https://stackoverflow.com/questions/38891654/get-current-screen-width-in-xamarin-forms
            // Get Metrics
            var metrics = DeviceDisplay.MainDisplayInfo;

            // Width (in pixels)
            var width = metrics.Width;

            // Height (in pixels)
            var height = metrics.Height;

            // Screen density
            density = metrics.Density;

            minx = 10;
            miny = 50;

            maxx = (int)(width - 50);
            maxy = (int)(height - 250);

            skWidth = 10;
            skLength = 10;


            paintDraw = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                StrokeCap = SKStrokeCap.Round,
                StrokeJoin = SKStrokeJoin.Round,
                StrokeWidth = 5
            };
            paintBox = new SKPaint
            {
                Style = SKPaintStyle.Stroke,
                Color = SKColors.Gray,
                StrokeCap = SKStrokeCap.Round,
                StrokeJoin = SKStrokeJoin.Round,
                StrokeWidth = 2
            };

            // create gridlines
            dimWidth = 5;
            dimLen = skLength;
            CalcGraphLineSpacing();

            if (maxx > 0 && maxy > 0 && smallIncrement > 0 && largeIncrement > 0)
            {
                completedGridlines.Clear();
                DrawGraphLines();
                canvasView.InvalidateSurface();
            }


        }

        protected override void OnAppearing()
        {
            base.OnAppearing();            

            SelectedColor = Color.Black;
        }

        // ======================================================
        // Add code for SkiaScene for pinch and zoom...
        private void OnSizeChanged(object sender, EventArgs eventArgs)
        {
            SetSceneCenter();
        }
        private void SetSceneCenter()
        {
            if (_scene == null)
            {
                return;
            }
            var centerPoint = new SKPoint(canvasView.CanvasSize.Width / 2, canvasView.CanvasSize.Height / 2);
            _scene.ScreenCenter = centerPoint;
        }
        private void InitSceneObjects()
        {

            //Create scene
            ISKSceneRenderer myRenderer = new SketchSceneRenderer()
            {
                gridLines = completedGridlines,
                skmatrixOut = skMatrix
            }; 

            _scene = new SKScene(myRenderer)
            {
                MaxScale = 10,
                MinScale = 0.3f
            };


            SetSceneCenter();
            _touchGestureRecognizer = new TouchGestureRecognizer();
            _sceneGestureResponder = new SceneGestureRenderingResponder(() => canvasView.InvalidateSurface(), _scene, _touchGestureRecognizer)
            {
                TouchManipulationMode = SkiaScene.TouchManipulation.TouchManipulationMode.IsotropicScale,
                MaxFramesPerSecond = 100,
            };
            _sceneGestureResponder.StartResponding();
        }

        void OnTouchEffectAction(object sender, TouchActionEventArgs args)
        {
            if (args.Type == TouchActionType.Pressed)
            {
                SKPoint pt_0 = new SKPoint();
                pt_0.X = args.Location.X;
                pt_0.Y = args.Location.Y;

                // Convert Xamarin.Forms point to pixels
                SKPoint pt = ConvertToGraphPoint(args.Location);

                Gridline gls = new Gridline(pt.X, pt.Y, pt.X + 50, pt.Y, Color.Black, 5);
                completedGridlines.Add(gls);


                canvasView.InvalidateSurface();
            }
        }
        SKPoint ConvertToGraphPoint(TouchTrackingPoint pt)
        {
            // for this demo:
            scale = 1;
            transX = 0;
            transY = 0;


            SKPoint point = new SKPoint((float)(canvasView.CanvasSize.Width * pt.X / canvasView.Width),
                               (float)(canvasView.CanvasSize.Height * pt.Y / canvasView.Height));

            // https://forums.xamarin.com/discussion/144189/skiasharp-skiascene-how-to-draw-at-the-right-place-after-doing-pan-and-or-zoom#latest
            // this does a Transform and Scale   x' =  ( x - TransX) / ScaleX
            point.X = (int)((point.X - transX) / scale);
            point.Y = (int)((point.Y - transY) / scale);

            return point;
        }

        void OnPaint(object sender, SKPaintSurfaceEventArgs args)
        {
            if (_scene == null)
            {
                InitSceneObjects();
            }
            SKImageInfo info = args.Info;
            SKSurface surface = args.Surface;
            canvas = surface.Canvas;
            _scene.Render(canvas);

        }

        private void CalcGraphLineSpacing()
        {
            if (dimLen > 0 && dimWidth > 0)
            {

                if (dimLen < dimWidth)
                {
                    // swap len and width
                    float h = dimLen;
                    dimLen = dimWidth;
                    dimWidth = h;
                }

                float numRows = dimLen + 2;
                float numCols = dimWidth + 2;


                float xInc = (maxx - 50) / numCols;
                float yInc = (maxy - 100) / numRows;
                if (xInc < yInc)
                {
                    largeIncrement = xInc;
                }
                else
                {
                    largeIncrement = yInc;
                }
                incrementMultiple = 4;
                smallIncrement = largeIncrement / 4;
                //Debug.WriteLine("mult=" + incrementMultiple + " large=" + largeIncrement + " small=" + smallIncrement);
            }
            else
            {
                largeIncrement = 0;
                smallIncrement = 0;
               // Debug.WriteLine("mult=" + incrementMultiple + " large=" + largeIncrement + " small=" + smallIncrement);
            }

            //smallIncrement = largeIncrement / 5; // for now - need to calc it later

        }
        private void DrawGraphLines()
        {
            completedGridlines.Clear();

            for (int ix = minx; ix <= maxx; ix += (int)largeIncrement)
            {
                Gridline gl = new Gridline(ix, miny, ix, maxy, Color.LightGreen, 3);
                completedGridlines.Add(gl);

                for (int i = 1; i < incrementMultiple; i++)
                {
                    float nextX = ix + i * smallIncrement;
                    Gridline gls = new Gridline(nextX, miny, nextX, maxy, Color.GreenYellow, 2);
                    completedGridlines.Add(gls);
                }
            }

            for (int iy = miny; iy <= maxy; iy += (int)largeIncrement)
            {
                Gridline gl = new Gridline(minx, iy, maxx, iy, Color.LightGreen, 2);
                completedGridlines.Add(gl);

                for (int i = 1; i < incrementMultiple; i++)
                {
                    float nextY = iy + i * smallIncrement;
                    Gridline gls = new Gridline(minx, nextY, maxx, nextY, Color.GreenYellow, 2);
                    completedGridlines.Add(gls);
                }
            }
            Debug.WriteLine("Draw Num completedGridlines=" + completedGridlines.Count());            


        }
        private void MenuSelect_Activated(object sender, EventArgs e)
        {
        }
        private void MenuText_Activated(object sender, EventArgs e)
        {
        }
        
    }
}