﻿using SkiaFail.Drawing;
using SkiaScene;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using System.Text;

namespace SkiaFail.Renderers
{

    public class SketchSceneRenderer : ISKSceneRenderer
    {
        public List<Gridline> gridLines { get; set; }

        public SKMatrix skmatrixIn { get; set; } 
        public SKMatrix skmatrixOut { get; set; } 

        public void Render(SKCanvas canvas, float angleInRadians, SKPoint center, float scale)
        {

            // this should be done After SketchView constructor so SketchScale should exist.
            App.Current.Properties["SketchScale"] = scale.ToString();
            App.Current.Properties["TransX"] = canvas.TotalMatrix.TransX.ToString();
            App.Current.Properties["TransY"] = canvas.TotalMatrix.TransY.ToString();

            skmatrixIn = canvas.TotalMatrix;
            //skmatrixOut.TransX = skmatrixIn.TransX;
            canvas.Clear(SKColors.White);

            SKPaint paint = new SKPaint
            {
                Color = SKColors.Black,
                TextSize = 32
            };
 
            foreach (Gridline gl in gridLines)
            {
                paint.Color = gl.StrokeColor.ToSKColor();
                paint.StrokeWidth = gl.StrokeWidth;
                canvas.DrawLine(gl.xs, gl.ys, gl.xe, gl.ye, paint);
            }       

        }
    }
}
