﻿using Xamarin.Forms;

namespace SkiaFail.Drawing
{
    public class Gridline
    {
        public float xs { set; get; }
        public float xe { set; get; }
        public float ys { set; get; }
        public float ye { set; get; }
        // canvas.DrawLine(nextX, miny, nextX, maxy, paintGreenYellow);
        public Color StrokeColor { set; get; }

        public float StrokeWidth { set; get; }

        public Gridline()
        {

        }
        public Gridline(float x0, float y0, float x1, float y1, Color color, float strokeWidth)
        {
            xs = x0;
            ys = y0;
            xe = x1;
            ye = y1;
            StrokeColor = color;
            StrokeWidth = strokeWidth;
        }
    }
}
